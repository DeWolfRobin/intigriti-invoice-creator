# intigriti-invoice-creator
An automatic invoice creator for intigriti.com

## Install
1. You need to be able to run a shell script (linux is optimal, wsl and mac will work too)
2. You need to be able to run a php server (go to the directory and enter `php -S 127.0.0.1:80`)
3. Create a file `username` & `password`
3. Insert login details in the files
3. Edit the details in `factuur.php`
4. Go to [localhost](http://localhost)

## Usage
1. Go to [localhost](http://localhost)
2. Select the payout
3. Press `ctrl + p`
4. Print to file
